package storage

import (
	"gitlab.com/djinni-project/backend/storage/mongodb"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/mongo"
	"go.opentelemetry.io/otel/trace"
)

type StorageI interface {
	Applicant() repo.ApplicantI
	Employer() 	repo.EmployerI
	User() 		repo.UserI
}

type StoragePg struct {
	applicantRepo repo.ApplicantI
	employerRepo  repo.EmployerI
	userRepo      repo.UserI
}

func NewStorage(db *mongo.Database, tracer trace.Tracer) StorageI {
	return &StoragePg{
		applicantRepo: mongodb.NewApplicant(db, tracer),
		employerRepo:  mongodb.NewEmployer(db, tracer),
		userRepo:      mongodb.NewUser(db, tracer),
	}
}

func (s *StoragePg) Applicant() repo.ApplicantI {
	return s.applicantRepo
}

func (s *StoragePg) Employer() repo.EmployerI {
	return s.employerRepo
}

func (s *StoragePg) User() repo.UserI {
	return s.userRepo
}
