package repo

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type EmployerI interface {
	Create(ctx context.Context, employer *Employer) (interface{}, error)
	GetByID(ctx context.Context, employerId string) (*Employer, error)
	GetByEmail(ctx context.Context, employerEmail string) (*Employer, error)
	UpdatePassword(ctx context.Context, employerId string, email string, password string) error
	Delete(ctx context.Context, email, id string) error
}

type Employer struct {
	Id                      primitive.ObjectID `bson:"_id"`
	FullName                string            `bson:"full_name"` // firstName + lastName
	Title                   *string            `bson:"title"`
	Email                   string             `bson:"email"`
	Password                string             `bson:"password"`
	Telegram                *string            `bson:"telegram"` // username
	PhoneNumber             *string            `bson:"phone_number"`
	LinkedInProfile         *string            `bson:"linkedin_profile"`
	ImageUrl                *string            `bson:"image_url"`
	Companyinfo             *CompanyInfo       `bson:"company_info"`
	Inboxes                 []*Inbox           `bson:"inboxes"`
	NotificationBy          *string            `bson:"notification_by"` // email or telegram
	Subscriptions           []*Subscription    `bson:"subscriptions"`
	SubscriptionApllicantId []string           `bson:"subscription_applicants_id"`
	Settings                *Setting           `bson:"settings"`
	Slug                    *string            `bson:"slug"`
	CreatedAt               time.Time          `bson:"created_at"`
}

type CompanyInfo struct {
	Id   primitive.ObjectID `bson:"_id"`  // CompanyId
	Role string             `bson:"role"` // "employer or employer_admin"
}

type Inbox struct {
	InboxId  string `bson:"inbox_id"`
	Note     string `bson:"note"`
	IsActive string `bson:"is_active"`
}

type Subscription struct {
	PrimaryRole  string     `bson:"primary_role"` // Golang or etc
	WorkExp      int64      `bson:"work_exp"`
	Salary       *EmpSalary `bson:"salary"`
	EnglishLevel string     `bson:"english_level"`
	Location     string     `bson:"location"`
	Keywords     []string   `bson:"keywords"`
	SearchMethod string     `bson:"search_method"` // basic search -> position, skill, city and type of employment; full-text search; title-ony-search -> title
}

type EmpSalary struct {
	From int64 `bson:"from"`
	To   int64 `bson:"to"`
}

type Setting struct {
	ApplicantSharedContact bool `bson:"applicant_shared_contact"`
	DjinniNews             bool `bson:"djinni_news"`
	FavouriteJobs          bool `bson:"favourite_jobs"`
}
