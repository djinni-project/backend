package repo

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserI interface {
	Create(ctx context.Context, user *User) (interface{}, error)
	GetByID(ctx context.Context, userId string) (*User, error)
	GetByEmail(ctx context.Context, userEmail string) (*User, error)
	UpdatePassword(ctx context.Context, userEmail, userId, password string) error
	Delete(ctx context.Context, email, id string) error
}

type User struct {
	Id        primitive.ObjectID `bson:"_id"`
	FirstName string             `bson:"first_name"`
	LastName  string             `bson:"last_name"`
	Email     string             `bson:"email"`
	Password  string             `bson:"password"`
	UserType  string             `bson:"user_type"` // superadmin, applicant and employer
	CreatedAt time.Time          `bson:"created_at"`
}
