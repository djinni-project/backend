package repo

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ApplicantI interface {
	Create(ctx context.Context, applicant *Applicant) (interface{}, error)
	GetByID(ctx context.Context, applicantID string) (*Applicant, error)
	GetByEmail(ctx context.Context, email string) (*Applicant, error)
	Delete(ctx context.Context, id, email string) error
	UpdateApplicantProfile(ctx context.Context, applicant *Applicant) error
}

type Applicant struct {
	Id                   primitive.ObjectID      `bson:"_id"`
	Profile              ApplicantProfile        `bson:"profile"`
	PhoneNumber          *string                 `bson:"phone_number"`
	GitHub               *string                 `bson:"github"`
	Portfolio            *string                 `bson:"portfolio"` // website
	CvUrl                *string                 `bson:"cv_url"`
	NotificationBy       *string                 `bson:"notification_by"` // email or telegram
	NewJobsNotification  bool                    `bson:"new_jobs_notification"`
	SearchJob            *string                 `bson:"search_job"` // active, passive, offline
	Subscriptions        []ApplicantSubscription `bson:"subscriptions"`
	SubscriptionsJobs    []string                `bson:"subscription_jobs_id"`
	StopList             ApplicantStopList       `bson:"stop_list"`
	Position             *string                 `bson:"position"`
	SalaryExpect         *int64                  `bson:"salary_expectation"`
	WorkExper            ApplicantWorkExp        `bson:"work_experience"`
	CountryRel           bool                    `bson:"country_relocation"` // if true that means applicant can relocate to another country
	CityRel              bool                    `bson:"city_relocation"`    // if true that means applicant can relocate to another city
	Cities               []string                `bson:"cities_relocation"`
	Skills               []string                `bson:"skills"`
	Category             *string                 `bson:"category"`
	EnglishLevel         *string                 `bson:"english_level"`     // begineer, elementary ...
	EmploymentOptions    []string                `bson:"employemt_options"` // remote, office, part_time, freelancer
	DoNotConsider        ApplicantDoNotConsider  `bson:"do_not_consider"`
	WorkExpDes           *string                 `bson:"work_exp_desciption"` // shoult write gt > 250
	Expectations         *string                 `bson:"expectations"`
	Accomplishments      *string                 `bson:"accomplishments"` // should fill gt > 250
	QuestionForEmployer  *string                 `bson:"question_for_employer"`
	PreferedLanguages    []string                `bson:"prefered_languages"`    // uzbek, english, russian
	DesiredCommunacation *string                 `bson:"desired_communacation"` // does not matter, djinni, email, phone, skype, whatsup, telegram, linkedin
	SavedJobs            []string                `bson:"saved_jobs"`
	Inboxes              []ApplicantInbox        `bson:"inboxes"`
	Slug                 *string                 `bson:"slug"`
	UpdateAt             *time.Time              `bson:"updated_at"`
	CreatedAt            time.Time               `bson:"created_at"`
}

type ApplicantProfile struct {
	FirstName    *string `bson:"first_name"`
	LastName     *string `bson:"last_name"`
	Email        string  `bson:"email"`
	Password     string  `bson:"password"`
	PhotoUrl     *string `bson:"photo_url"`
	ProfieViews  []*ApplicantProfileView
	SocialMedias ApplicantSocialMedias
}

type ApplicantProfileView struct {
	EmployerFullName string     `bson:"employer_fullname"`
	CompanyName      string     `bson:"company_name"`
	IsActiveProfile  bool       `bson:"is_active_profile"`
	SeenAt           *time.Time `bson:"seen_at"`
}

type ApplicantSocialMedias struct {
	Email    *string `bson:"email"`
	Skype    *string `bson:"skype"`
	Telegram *string `bson:"telegram_username"`
	WhatsUp  *string `bson:"whats_up"`
	LinkedIn *string `bson:"linkedin"`
}

type ApplicantSubscription struct {
	IsActive    bool   `bson:"is_active"` // default true
	PrimaryRole string `bson:"primary_role"`
	Salary      int64  `bson:"salary"` // if 0 does not matter or gt matter
	Keyword     string `bson:"keyword"`
}

type ApplicantStopList struct {
	BlockedCompanies  []string `bson:"blocked_companies"`
	BlockedRecruiters []string `bson:"blocked_recruiters"`
}

type ApplicantWorkExp struct {
	During *int64  `bson:"during"`
	Period *string `bson:"period"` // month of year
}

type ApplicantRelocationCites struct {
	Relocation bool     `bson:"relocation"`
	City       []string `bson:"cities"`
}

type ApplicantDoNotConsider struct {
	Domains      []string `bson:"domains"`       // "adult", "gambling", "dating", "gamedev", "blockchain/crypto"
	CompanyTypes []string `bson:"company_types"` // "agency", "outsource", "outstaff", "product", "startup
}

type ApplicantInbox struct {
	InboxId string `bson:"inbox_id"`
	Type    string `bson:"type"` // active, favourites, archive,
}
