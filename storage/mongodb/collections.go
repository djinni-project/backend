package mongodb

import (
	"errors"

	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/mongo"
	"go.opentelemetry.io/otel/trace"
)

const (
	applicants = "applicants"
	employer   = "employers"
	user = "users"
)

var (
	ErrUserAlreadyExists = errors.New("user with such email already exists")
	ErrUserNotFound      = errors.New("user doesn't exists")
)

type applicantRepo struct {
	col    *mongo.Collection
	tracer trace.Tracer
}

type employerRepo struct {
	col    *mongo.Collection
	tracer trace.Tracer
}

type userRepo struct {
	col *mongo.Collection
	tracer trace.Tracer
}

func NewApplicant(db *mongo.Database, tracer trace.Tracer) repo.ApplicantI {
	return &applicantRepo{
		col:    db.Collection(applicants),
		tracer: tracer,
	}
}

func NewEmployer(db *mongo.Database, tracer trace.Tracer) repo.EmployerI {
	return &employerRepo{
		col: db.Collection(employer),
		tracer: tracer,
	}
}

func NewUser(db *mongo.Database, tracer trace.Tracer) repo.UserI {
	return &userRepo{
		col: db.Collection(user),
		tracer: tracer,
	}
}