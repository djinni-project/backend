package mongodb_test

import (
	"context"
	"testing"

	"github.com/go-faker/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/djinni-project/backend/pkg/utils"
	"gitlab.com/djinni-project/backend/storage/repo"
)

func createUser(t *testing.T) *repo.User {
	password := "isma02"
	hashedPassword, err := utils.HashPassword(password)
	assert.NoError(t, err)

	user := repo.User{
		Email:    "rismoiljon33@gmail.com",
		Password: hashedPassword,
		UserType: "applicant",
	}

	res, err := dbManager.User().Create(context.Background(), &user)
	assert.NoError(t, err)
	assert.NotEmpty(t, res)

	return &user
}

func deleteUser(t *testing.T, email, id string) {
	err := dbManager.User().Delete(context.Background(), email, id)
	assert.NoError(t, err)
}

func TestCreateUser(t *testing.T) {
	createUser(t)
}

func TestDeleteUser(t *testing.T) {
	user := createUser(t)

	deleteUser(t, user.Email, user.Id.Hex())
}

func TestGetUserById(t *testing.T) {
	user := createUser(t)

	res, err := dbManager.User().GetByID(context.Background(), user.Id.Hex())
	assert.NoError(t, err)
	assert.NotEmpty(t, res)
}

func TestUpdateUserPassword(t *testing.T) {
	user := createUser(t)

	hashedPassword, err := utils.HashPassword(faker.Word())
	assert.NoError(t, err)

	err = dbManager.User().UpdatePassword(context.Background(), user.Email, user.Id.Hex(), hashedPassword)
	assert.NoError(t, err)

}

func TestGetUserByEmail(t *testing.T) {
	user := createUser(t)

	res, err := dbManager.User().GetByEmail(context.Background(), user.Email)
	assert.NoError(t, err)
	assert.NotEmpty(t, res)
}
