package mongodb

import (
	"context"
	"errors"

	"gitlab.com/djinni-project/backend/pkg/mongodb"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (c *employerRepo) Create(ctx context.Context, employer *repo.Employer) (interface{}, error) {
	cx, span := c.tracer.Start(ctx, "employer.Create")
	defer span.End()
	res, err := c.col.InsertOne(cx, employer)
	if err != nil {
		if mongodb.IsDuplicate(err) {
			return nil, ErrUserAlreadyExists
		}
		return nil, err
	}

	return res.InsertedID, nil
}

func (c *employerRepo) GetByID(ctx context.Context, employerId string) (*repo.Employer, error) {
	cx, span := c.tracer.Start(ctx, "employer.GetByID")
	defer span.End()

	id, err := primitive.ObjectIDFromHex(employerId)
	if err != nil {
		return nil, err
	}

	var res repo.Employer
	err = c.col.FindOne(cx, bson.M{"_id": id}).Decode(&res)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mongo.ErrNoDocuments
		}

		return nil, err
	}

	return &res, nil
}

func (c *employerRepo) GetByEmail(ctx context.Context, employerEmail string) (*repo.Employer, error) {
	cx, span := c.tracer.Start(ctx, "employer.GetByEmail")
	defer span.End()

	var res repo.Employer
	err := c.col.FindOne(cx, bson.M{"email": employerEmail}).Decode(&res)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mongo.ErrNoDocuments
		}

		return nil, err
	}
	return &res, nil
}

func (c *employerRepo) UpdatePassword(ctx context.Context, employerId string, email string, password string) error {
	cx, span := c.tracer.Start(ctx, "employer.UpdatePassword")
	defer span.End()

	id, err := primitive.ObjectIDFromHex(employerId)
	if err != nil {
		return err
	}

	res, err := c.col.UpdateOne(cx, bson.M{"email": email, "_id": id}, bson.M{"$set": bson.M{"password": password}})
	if err != nil {
		return err
	}
	if res.ModifiedCount == 0 {
		return mongo.ErrNoDocuments
	}

	return err

}

func (c *employerRepo) Delete(ctx context.Context, id, email string) error {
	cx, span := c.tracer.Start(ctx, "employer.Delete")
	defer span.End()

	employerId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	res, err := c.col.DeleteOne(cx, bson.M{"email": email, "_id": employerId})
	if err != nil {
		return err
	}

	if res.DeletedCount == 0 {
		return mongo.ErrNoDocuments
	}

	return nil

}