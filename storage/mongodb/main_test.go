package mongodb_test

import (
	"context"
	"os"
	"testing"

	"gitlab.com/djinni-project/backend/config"
	"gitlab.com/djinni-project/backend/pkg/jaeger"
	"gitlab.com/djinni-project/backend/pkg/logger"
	"gitlab.com/djinni-project/backend/pkg/mongodb"
	"gitlab.com/djinni-project/backend/pkg/test_container"
	"gitlab.com/djinni-project/backend/storage"
	"go.opentelemetry.io/otel"
)

var (
	dbManager storage.StorageI
)

func TestMain(m *testing.M) {
	logger.Init()
	log := logger.GetLogger()

	cfg := config.Load("./../..")

	tContainer, err := test_container.SetupMongoDBContainer(context.Background())
	if err != nil {
		log.Fatal("Error while making test container", err)
	}

	mongoClient, err := mongodb.NewClient(cfg.MongoDB.Url, tContainer.User, tContainer.Pass)
	if err != nil {
		log.Fatal("Error while making client to mongodb", err)
	}

	db := mongoClient.Database(tContainer.Name)

	// initialing jaeger
	tp, err := jaeger.InitJaeger(&cfg)
	if err != nil {
		log.Fatal("cannot create tracer", err)
	}

	// initializing open telemetry
	otel.SetTracerProvider(tp)
	tr := tp.Tracer("main-otel")
	log.Info("Opentracing connected")

	dbManager = storage.NewStorage(db, tr)

	os.Exit(m.Run())
}
