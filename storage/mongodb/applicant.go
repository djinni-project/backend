package mongodb

import (
	"context"
	"errors"

	"gitlab.com/djinni-project/backend/pkg/mongodb"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (c *applicantRepo) Create(ctx context.Context, applicant *repo.Applicant) (interface{}, error) {
	cx, span := c.tracer.Start(ctx, "applicant.Create")
	defer span.End()

	res, err := c.col.InsertOne(cx, applicant)
	if err != nil {
		if mongodb.IsDuplicate(err) {
			return nil, ErrUserAlreadyExists
		}
		return nil, err
	}

	return res.InsertedID, nil
}

func (c *applicantRepo) GetByID(ctx context.Context, applicantID string) (*repo.Applicant, error) {
	cx, span := c.tracer.Start(ctx, "applicant.GetByID")
	defer span.End()

	var applicant repo.Applicant
	id, err := primitive.ObjectIDFromHex(applicantID)
	if err != nil {
		return nil, err
	}

	if err = c.col.FindOne(cx, bson.M{"_id": id}).Decode(&applicant); err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mongo.ErrNoDocuments
		}
		return nil, err
	}

	return &applicant, nil
}

func (c *applicantRepo) GetByEmail(ctx context.Context, applicantEmail string) (*repo.Applicant, error) {
	cx, span := c.tracer.Start(ctx, "applicant.GetByEmail")
	defer span.End()

	var applicant repo.Applicant

	if err := c.col.FindOne(cx, bson.M{"profile.email": applicantEmail}).Decode(&applicant); err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mongo.ErrNoDocuments
		}
		return nil, err
	}

	return &applicant, nil
}

func (c *applicantRepo) Delete(ctx context.Context, id, email string) error {
	cx, span := c.tracer.Start(ctx, "applicant.Delete")
	defer span.End()

	applicantID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	res, err := c.col.DeleteOne(cx, bson.M{"_id": applicantID, "profile.email": email})
	if err != nil {
		return err
	}
	if res.DeletedCount == 0 {
		return mongo.ErrNoDocuments
	}

	return nil
}

func (c *applicantRepo) UpdateApplicantProfile(ctx context.Context, applicant *repo.Applicant) error {
	cx, span := c.tracer.Start(ctx, "applicant.UpdateApplicantProfile")
	defer span.End()

	updateQuery := handleUpdateApplicantProfileQuery(applicant)

	_, err := c.col.UpdateOne(cx, bson.M{"_id": applicant.Id, "profile.email": applicant.Profile.Email}, bson.M{"$set": updateQuery})

	return err
}

// skills -> Go/Golang, PostgreSQL, MongoDb
// database skills -> Go/Golang, PostgreSQL, MongoDb
// a = Go/Golang, PostgreSQL, MongoDb
// b = MongoDb
// a = b
// a = Mongodb
