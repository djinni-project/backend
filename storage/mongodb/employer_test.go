package mongodb_test

import (
	"context"
	"testing"
	"time"

	"github.com/go-faker/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/djinni-project/backend/pkg/utils"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func createEmployer(t *testing.T) *repo.Employer {
	hashedPassword, err := utils.HashPassword(faker.Word())
	assert.NoError(t, err)

	employer := repo.Employer{
		Id:        primitive.NewObjectID(),
		Email:     faker.Email(),
		Password:  hashedPassword,
		CreatedAt: time.Now(),
	}
	res, err := dbManager.Employer().Create(context.Background(), &employer)
	assert.NoError(t, err)
	assert.NotEmpty(t, res)

	return &employer
}

func deleteEmployer(t *testing.T, id, email string) {
	err := dbManager.Employer().Delete(context.Background(), id, email)
	assert.NoError(t, err)
}

func TestCreateEmployer(t *testing.T) {
	createEmployer(t)
}

func TestGetEmployerById(t *testing.T) {
	employer := createEmployer(t)

	res, err := dbManager.Employer().GetByID(context.Background(), employer.Id.Hex())

	assert.NoError(t, err)
	assert.NotEmpty(t, res)
}

func TestGetEmployerByEmail(t *testing.T) {
	employer := createEmployer(t)

	res, err := dbManager.Employer().GetByEmail(context.Background(), employer.Email)

	assert.NoError(t, err)
	assert.NotEmpty(t, res)
}

func TestUpdatePassword(t *testing.T) {
	employer := createEmployer(t)

	hashedPassword, err := utils.HashPassword(faker.Word())
	assert.NoError(t, err)

	err = dbManager.Employer().UpdatePassword(context.Background(), employer.Id.Hex(), employer.Email, hashedPassword)
	assert.NoError(t, err)
}

func TestDeleteEmployer(t *testing.T) {
	employer := createEmployer(t)
	deleteEmployer(t, employer.Id.Hex(), employer.Email)

}
