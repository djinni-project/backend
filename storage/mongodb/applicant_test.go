package mongodb_test

// import (
// 	"context"
// 	"testing"
// 	"time"

// 	"github.com/go-faker/faker/v4"
// 	"github.com/stretchr/testify/assert"
// 	"gitlab.com/djinni-project/backend/storage/repo"
// 	"go.mongodb.org/mongo-driver/bson/primitive"
// )

// func createApplicant(t *testing.T) *repo.Applicant {
// 	applicant := repo.Applicant{
// 		Id: primitive.NewObjectID(),
// 		Profile: &repo.ApplicantProfile{
// 			Email: faker.Email(),
// 		},
// 		CreatedAt: time.Now(),
// 	}
// 	res, err := dbManager.Applicant().Create(context.Background(), &applicant)
// 	assert.NoError(t, err)
// 	assert.NotEmpty(t, res)

// 	return &applicant
// }

// func deleteApplicant(t *testing.T, id, email string) {
// 	err := dbManager.Applicant().Delete(context.Background(), id, email)
// 	assert.NoError(t, err)
// }

// func TestCreateApplicant(t *testing.T) {
// 	createApplicant(t)
// }

// func TestGetApplicantByID(t *testing.T) {
// 	applicant := createApplicant(t)

// 	res, err := dbManager.Applicant().GetByID(context.Background(), applicant.Id.Hex())

// 	assert.NoError(t, err)
// 	assert.NotEmpty(t, res)
// }

// func TestGetApplicantByEmail(t *testing.T) {
// 	applicant := createApplicant(t)

// 	res, err := dbManager.Applicant().GetByEmail(context.Background(), applicant.Profile.Email)

// 	assert.NoError(t, err)
// 	assert.NotEmpty(t, res)
// }

// func TestDeleteApplicant(t *testing.T) {
// 	applicant := createApplicant(t)
// 	deleteApplicant(t, applicant.Id.Hex(), applicant.Profile.Email)
// }
