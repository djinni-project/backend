package mongodb

import (
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson"
)

// This func for making bson.M update query of applicant profile
func handleUpdateApplicantProfileQuery(applicant *repo.Applicant) bson.M {
	updateQuery := make(bson.M, 0)

	if applicant.Position != nil {
		updateQuery["position"] = *applicant.Position
	}
	if applicant.SalaryExpect != nil {
		updateQuery["salary_expectation"] = *applicant.SalaryExpect
	}
	if applicant.WorkExper.During != nil && applicant.WorkExper.Period != nil {
		updateQuery["work_experience.during"] = applicant.WorkExper.During
		updateQuery["work_experience.period"] = applicant.WorkExper.Period // perio = {years or months}
	}
	if applicant.CountryRel {
		updateQuery["country_relocation"] = true
	} else {
		updateQuery["country_relocation"] = false
	}
	if applicant.CityRel {
		updateQuery["city_relocation"] = true
	} else {
		updateQuery["city_relocation"] = false
	}
	if applicant.Cities != nil {
		updateQuery["cities_relocation"] = applicant.Cities
	}
	if applicant.Skills != nil {
		updateQuery["skills"] = applicant.Skills
	}
	if applicant.Category != nil {
		updateQuery["category"] = applicant.Category
	}
	if applicant.EnglishLevel != nil {
		updateQuery["english_level"] = applicant.EnglishLevel
	}
	if applicant.EmploymentOptions != nil {
		updateQuery["employemt_options"] = applicant.EmploymentOptions
	}
	if applicant.DoNotConsider.Domains != nil {
		updateQuery["do_not_consider.domains"] = applicant.DoNotConsider.Domains
	}
	if applicant.DoNotConsider.CompanyTypes != nil {
		updateQuery["do_not_consider.company_types"] = applicant.DoNotConsider.CompanyTypes
	}
	if applicant.WorkExpDes != nil {
		updateQuery["work_exp_desciption"] = applicant.WorkExpDes
	}
	if applicant.Expectations != nil {
		updateQuery["expectations"] = applicant.Expectations
	}
	if applicant.Accomplishments != nil {
		updateQuery["accomplishments"] = applicant.Accomplishments
	}
	if applicant.QuestionForEmployer != nil {
		updateQuery["question_for_employer"] = applicant.QuestionForEmployer
	}
	if applicant.PreferedLanguages != nil {
		updateQuery["prefered_languages"] = applicant.PreferedLanguages
	}
	if applicant.DesiredCommunacation != nil {
		updateQuery["desired_communacation"] = applicant.DesiredCommunacation
	}

	return updateQuery
}
