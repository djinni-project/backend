package mongodb

import (
	"context"
	"errors"

	"gitlab.com/djinni-project/backend/pkg/mongodb"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (c *userRepo) Create(ctx context.Context, user *repo.User) (interface{}, error) {
	cx, span := c.tracer.Start(ctx, "user.Create")
	defer span.End()
	user.Id = primitive.NewObjectID()

	res, err := c.col.InsertOne(cx, user)
	if err != nil {
		if mongodb.IsDuplicate(err) {
			return nil, ErrUserAlreadyExists
		}
		return nil, err
	}

	return res.InsertedID, nil
}

func (c *userRepo) GetByID(ctx context.Context, id string) (*repo.User, error) {
	cx, span := c.tracer.Start(ctx, "user.Get")
	defer span.End()

	userId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	var res repo.User

	err = c.col.FindOne(cx, bson.M{"_id": userId}).Decode(&res)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mongo.ErrNoDocuments
		}
		return nil, err
	}
	return &res, nil
}

func (c *userRepo) GetByEmail(ctx context.Context, email string) (*repo.User, error) {
	cx, span := c.tracer.Start(ctx, "user.GetByEmail")
	defer span.End()

	var res repo.User

	err := c.col.FindOne(cx, bson.M{"email": email}).Decode(&res)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mongo.ErrNoDocuments
		}
		return nil, err
	}

	return &res, nil
}

func (c *userRepo) UpdatePassword(ctx context.Context, email, id, password string) error {
	cx, span := c.tracer.Start(ctx, "user.UpdatePassword")
	defer span.End()

	userId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	res, err := c.col.UpdateOne(cx, bson.M{"email": email, "_id": userId}, bson.M{"$set": bson.M{"password": password}})
	if err != nil {
		return err
	}

	if res.ModifiedCount == 0 {
		return mongo.ErrNoDocuments
	}

	return err
}

func (c *userRepo) Delete(ctx context.Context, email, id string) error {
	cx, span := c.tracer.Start(ctx, "user.Delete")
	defer span.End()

	userId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	res, err := c.col.DeleteOne(cx, bson.M{"email": email, "_id": userId})
	if err != nil {
		return err
	}

	if res.DeletedCount == 0 {
		return mongo.ErrNoDocuments
	}

	return nil
}