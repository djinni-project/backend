.SILENT:
.PHONY:

run:
	go run cmd/main.go

tidy:
	go mod tidy
	go mod vendor

swag-init:
	swag init -g api/api.go -o api/docs

test:
	go test --short -coverprofile=cover.out -v ./...
	make test.coverage

test.coverage:
	go tool cover -func=cover.out | grep "total"

lint:
	golangci-lint run