package test_container

import (
	"context"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

type mongodbContainer struct {
	testcontainers.Container
	Host string
	Port string
	Name string
	User string
	Pass string
}

// SetupMongoDBContainer runs mongo container and returns connection details
func SetupMongoDBContainer(ctx context.Context) (*mongodbContainer, error) {
	req := testcontainers.ContainerRequest{
		Image:        "mongo",
		ExposedPorts: []string{"27017/tcp"},
		WaitingFor: wait.ForAll(
			wait.ForLog("Waiting for connections"),
			wait.ForListeningPort("27017/tcp"),
		),
		Env: map[string]string{
			"MONGO_INITDB_ROOT_USERNAME": "mongo",
			"MONGO_INITDB_ROOT_PASSWORD": "mongo",
			"MONGO_INITDB_DATABASE":      "test_db",
		},
	}

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	mappedPort, err := container.MappedPort(ctx, "27017")
	if err != nil {
		return nil, err
	}

	hostIP, err := container.Host(ctx)
	if err != nil {
		return nil, err
	}

	return &mongodbContainer{
		Container: container,
		Host:      hostIP,
		Port:      mappedPort.Port(),
		Name:      "mongo",
		Pass:      "mongo",
		User:      "mongo",
	}, nil
}
