package test_container

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetupTestDB(t *testing.T) {
	container, err := SetupMongoDBContainer(context.Background())
	assert.NoError(t, err)
	assert.NotEmpty(t, container)
}