package email

import (
	"bytes"
	"fmt"
	"html/template"
	"net/smtp"

	"gitlab.com/djinni-project/backend/config"
)

type SendEmailRequest struct {
	To      []string
	Type    string
	Body    map[string]string
	Subject string
}

const (
	VerificationEmail   = "verification_email"
	ForgotPasswordEmail = "forgot_password_email"
)

func SendEmail(cfg *config.Config, req *SendEmailRequest) error {
	from := cfg.Smtp.Sender
	to := req.To

	password := cfg.Smtp.Password

	var body bytes.Buffer

	templatePath := getTemplatePath(req.Type)
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		return err
	}
	t.Execute(&body, req.Body)

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := fmt.Sprintf("Subject: %s\n", req.Subject)
	msg := []byte(subject + mime + body.String())

	auth := smtp.PlainAuth("", from, password, "smtp.gmail.com")
	err = smtp.SendMail("smtp.gmail.com:587", auth, from, to, msg)
	if err != nil {
		return err
	}

	return nil
}

func getTemplatePath(emailType string) string {
	switch emailType {
	case VerificationEmail:
		return "./templates/verification_email.html"
	case ForgotPasswordEmail:
		return "./templates/forgot_password_email.html"
	}
	return ""
}

func SendCodeToEmaill(to []string, message []byte) error {
	from := "ismoiljonrustamov11@gmail.com"
	password := "mtczswqpoyeipzur"

	// smtp server configuration.
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Sending email.
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	return err

}
