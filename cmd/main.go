package main

import (
	"fmt"

	"gitlab.com/djinni-project/backend/api"
	_ "gitlab.com/djinni-project/backend/api/docs"
	"gitlab.com/djinni-project/backend/config"
	"gitlab.com/djinni-project/backend/pkg/jaeger"
	"gitlab.com/djinni-project/backend/pkg/logger"
	"gitlab.com/djinni-project/backend/pkg/mongodb"
	"gitlab.com/djinni-project/backend/storage"
	"gitlab.com/djinni-project/backend/storage/redis"

	r "github.com/gomodule/redigo/redis"
	"go.opentelemetry.io/otel"
)

func main() {
	logger.Init()
	log := logger.GetLogger()

	// intiliazing config
	cfg := config.Load(".")

	// making client to mongodb
	mongoClient, err := mongodb.NewClient(cfg.MongoDB.Url, cfg.MongoDB.Username, cfg.MongoDB.Password)
	if err != nil {
		log.Fatal("Error while making client to mongodb", err)
	}

	db := mongoClient.Database(cfg.MongoDB.Database)

	// initialing jaeger
	tp, err := jaeger.InitJaeger(&cfg)
	if err != nil {
		log.Fatal("cannot create tracer", err)
	}

	// initializing open telemetry
	otel.SetTracerProvider(tp)
	tr := tp.Tracer("main-otel")
	log.Info("Opentracing connected")

	pool := r.Pool{
		MaxIdle:   80,
		MaxActive: 12000,
		Dial: func() (r.Conn, error) {
			c, err := r.Dial("tcp", fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort))
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}
	strg := storage.NewStorage(db, tr)
	redi := redis.NewRedisRepo(&pool)

	apiServer := api.New(&api.RoutetOptions{
		Cfg:     &cfg,
		Storage: strg,
		Tracer:  tr,
		Log:     log,
		Redis:   redi,
	})

	if err = apiServer.Run(cfg.HttpPort); err != nil {
		log.Errorf("failed to run server: %s", err)
	}

}
