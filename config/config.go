package config

import (
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	HttpPort                   string
	TokenSecretKey             string
	FrontendUrl                string
	AccessTokenExpireDuration  time.Duration
	ResetPasswordTokenDuration time.Duration
	MongoDB                    MongoDB
	Jaeger                     Jaeger
	Smtp                       Smtp
	RedisHost                  string
	RedisPort                  string
}

type MongoDB struct {
	Url      string
	Username string
	Password string
	Database string
}

type Jaeger struct {
	Host        string
	ServiceName string
}

type Smtp struct {
	Sender   string
	Password string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		RedisHost: conf.GetString("REDIS_HOST"),
		RedisPort: conf.GetString("REDIS_PORT"),
		HttpPort:                   conf.GetString("HTTP_PORT"),
		TokenSecretKey:             conf.GetString("TOKEN_SECRET_KEY"),
		AccessTokenExpireDuration:  conf.GetDuration("ACCESS_TOKEN_DURATION"),
		ResetPasswordTokenDuration: conf.GetDuration("RESET_PASSWORD_TOKEN_DURATION"),
		FrontendUrl:                conf.GetString("FRONTEND_URL"),
		MongoDB: MongoDB{
			Url:      conf.GetString("MONGODB_URL"),
			Username: conf.GetString("MONGODB_USERNAME"),
			Password: conf.GetString("MONGODB_PASSWORD"),
			Database: conf.GetString("MONGODB_DATABASE"),
		},
		Jaeger: Jaeger{
			Host:        conf.GetString("JAEGER_HOST"),
			ServiceName: conf.GetString("JAEGER_SERVICE_NAME"),
		},
		Smtp: Smtp{
			Sender:   conf.GetString("SMTP_SENDER"),
			Password: conf.GetString("SMTP_PASSWORD"),
		},
	}
	return cfg
}
