package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/djinni-project/backend/api/models"
	"gitlab.com/djinni-project/backend/config"
	"gitlab.com/djinni-project/backend/pkg/logger"
	"gitlab.com/djinni-project/backend/storage"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.opentelemetry.io/otel/trace"
)

type handlerV1 struct {
	cfg    *config.Config
	strg   storage.StorageI
	tracer trace.Tracer
	log    logger.Logger
	redis  repo.RedisRepo
}

type HandlerV1Options struct {
	Cfg    *config.Config
	Strg   *storage.StorageI
	Tracer trace.Tracer
	Log    logger.Logger
	Redis  repo.RedisRepo
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:    options.Cfg,
		strg:   *options.Strg,
		tracer: options.Tracer,
		log:    options.Log,
		redis:  options.Redis,
	}
}

func (h *handlerV1) HandleHTTPError(c *gin.Context, code int, message string, err error) bool {
	if err != nil {
		h.log.Errorf("Error -> %v", err)
		c.JSON(code, models.FailureResponse{
			Message: message,
			Error:   err.Error(),
		})
		return true
	}
	return false
}

func (h *handlerV1) HttpError(c *gin.Context, code int, message string, err error) {
	h.log.Errorf("Error -> %v", err)
	c.JSON(code, models.FailureResponse{
		Message: message,
		Error:   err.Error(),
	})
}

func isValidPassword(password string) bool {
	var capitalLetter, smallLetter, number bool
	for i := 0; i < len(password); i++ {
		if password[i] >= 65 && password[i] <= 90 {
			capitalLetter = true
		} else if password[i] >= 97 && password[i] <= 122 {
			smallLetter = true
		} else if password[i] >= 48 && password[i] <= 57 {
			number = true
		}
	}
	return capitalLetter && smallLetter && number
}

func handleApplicantProfile(aProfile *models.ApplicantProfile) (*repo.Applicant, error) {
	id, err := primitive.ObjectIDFromHex(aProfile.ID)
	if err != nil {
		return nil, err
	}
	applicant := repo.Applicant{
		Id:           id,
		Profile:      repo.ApplicantProfile{Email: aProfile.Email},
		Position:     &aProfile.Position,
		SalaryExpect: &aProfile.SalaryExpectation,
		Skills:       aProfile.Skills,
		Category:     &aProfile.Category,
	}

	if aProfile.WorkExperience.During != 0 && aProfile.WorkExperience.Period != "" {
		applicant.WorkExper = repo.ApplicantWorkExp{
			During: &aProfile.WorkExperience.During,
			Period: &aProfile.WorkExperience.Period,
		}
	}
	if aProfile.CitiesRel != nil {
		applicant.Cities = aProfile.CitiesRel
	}
	if aProfile.EnglishLevel != "" {
		applicant.EnglishLevel = &aProfile.EnglishLevel
	}
	if aProfile.EmploymentOptions != nil {
		applicant.EmploymentOptions = aProfile.EmploymentOptions
	}
	if aProfile.IDontConsider.Domains != nil && aProfile.IDontConsider.CompanyTypes != nil {
		applicant.DoNotConsider = repo.ApplicantDoNotConsider{
			Domains:      aProfile.IDontConsider.Domains,
			CompanyTypes: aProfile.IDontConsider.CompanyTypes,
		}
	} else if aProfile.IDontConsider.Domains != nil && aProfile.IDontConsider.CompanyTypes == nil {
		applicant.DoNotConsider = repo.ApplicantDoNotConsider{
			Domains: aProfile.IDontConsider.Domains,
		}
	} else {
		applicant.DoNotConsider = repo.ApplicantDoNotConsider{
			CompanyTypes: aProfile.IDontConsider.CompanyTypes,
		}
	}
	if aProfile.WorkExperienceDes != "" {
		applicant.WorkExpDes = &aProfile.WorkExperienceDes
	}
	if aProfile.Expectations != "" {
		applicant.Expectations = &aProfile.Expectations
	}
	if aProfile.Accomplishments != "" {
		applicant.Accomplishments = &aProfile.Accomplishments
	}
	if aProfile.QuestionsForEmployer != "" {
		applicant.QuestionForEmployer = &aProfile.QuestionsForEmployer
	}
	if aProfile.PreferedLanguages != nil {
		applicant.PreferedLanguages = aProfile.PreferedLanguages
	}
	if aProfile.DesiredCommunacation != "" {
		applicant.DesiredCommunacation = &aProfile.DesiredCommunacation
	}

	return &applicant, nil
}
