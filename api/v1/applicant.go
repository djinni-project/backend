package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/djinni-project/backend/api/models"
)

// UpdateApplicantProfile
// @Summary	UpdateApplicantProfile
// @Tags applicant
// @Description this api for update applicant profile
// @Accept json
// @Produce json
// @Param data body models.ApplicantProfile true "data"
// @Succes 200 {object}  models.ResponseOK
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /applicant/profile  [put]
func (h *handlerV1) UpdateApplicantProfile(c *gin.Context) {
	ctx, span := h.tracer.Start(c, "applicant.UpdateApplicantProfile")
	defer span.End()
	var req models.ApplicantProfile
	if err := c.ShouldBindJSON(&req); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	applicant, err := handleApplicantProfile(&req)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageInternalServerError, err) {
		return
	}

	err = h.strg.Applicant().UpdateApplicantProfile(ctx, applicant)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: models.MessageApplicantSuccessFullyUpdated,
	})
}
