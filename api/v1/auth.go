package v1

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/mail"
	"time"

	"github.com/gin-gonic/gin"
	r "github.com/gomodule/redigo/redis"
	"gitlab.com/djinni-project/backend/api/models"
	"gitlab.com/djinni-project/backend/pkg/email"
	"gitlab.com/djinni-project/backend/pkg/utils"
	"gitlab.com/djinni-project/backend/storage/repo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Login godoc
// @Summary Login user
// @Tags auth
// @Description login user, returns user data and token
// @Accept json
// @Produce json
// @Param data body models.LoginRequest true "data"
// @Success 200 {object} models.LoginResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /auth/login [post]
func (h *handlerV1) Login(c *gin.Context) {
	ctx, span := h.tracer.Start(c, "auth.Login")
	defer span.End()

	var req models.LoginRequest
	if err := c.ShouldBindJSON(&req); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	// getting user by email in order to check user exists
	user, err := h.strg.User().GetByEmail(ctx, req.Email)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			h.HttpError(c, http.StatusNotFound, models.MessageUserNotExists, err)
			return
		}
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	// checking email and password
	if err := utils.CheckPassword(req.Password, user.Password); err != nil || user.Email != req.Email {
		h.HttpError(c, http.StatusUnauthorized, models.MessageEmailOrPasswordIsNotTrue, errors.New(models.MessageEmailOrPasswordIsNotTrue))
		return
	}

	m := map[string]interface{}{
		"id":        user.Id.Hex(),
		"login":     user.Email,
		"user_type": user.UserType,
	}

	accessToken, expiresAt, err := utils.GenerateJWT(m, h.cfg.AccessTokenExpireDuration, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageGeneratingToken, err) {
		return
	}

	response := models.LoginResponse{
		UserInfo: models.User{
			ID:        user.Id.Hex(),
			FirstName: user.FirstName,
			LastName:  user.LastName,
			Email:     user.Email,
			UserType:  user.UserType,
			CreatedAt: user.CreatedAt,
		},
		AccessToken: accessToken,
		ExpiresAt:   expiresAt,
	}

	c.JSON(http.StatusOK, response)
}

// Forgot Password godoc
// @Security ApiKeyAuth
// @Summary Forgot Password
// @Tags auth
// @Description send forgot-password link to user's email
// @Accept json
// @Produce json
// @Param data body models.ForgotPasswordRequest true "data"
// @Success 200 {object} models.ResponseOK
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /auth/forgot-password [post]
func (h *handlerV1) ForgotPassword(c *gin.Context) {
	ctx, span := h.tracer.Start(c, "auth.ForgotPassword")
	defer span.End()

	var req models.ForgotPasswordRequest
	if err := c.ShouldBindJSON(&req); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	_, err := mail.ParseAddress(req.Email)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageInternalServerError, err) {
		return
	}

	// getting user by email in order to check user exists
	user, err := h.strg.User().GetByEmail(ctx, req.Email)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			h.HttpError(c, http.StatusNotFound, models.MessageUserNotExists, err)
			return
		}
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	m := map[string]interface{}{
		"id":        user.Id.Hex(),
		"login":     user.Email,
		"user_type": user.UserType,
	}

	resetToken, _, err := utils.GenerateJWT(m, h.cfg.ResetPasswordTokenDuration, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageGeneratingToken, err) {
		return
	}

	var body = map[string]string{
		"link": h.cfg.FrontendUrl + "/create-password?token=" + resetToken,
	}

	email.SendEmail(h.cfg, &email.SendEmailRequest{
		To:      []string{req.Email},
		Type:    email.ForgotPasswordEmail,
		Body:    body,
		Subject: "Reset your password",
	})

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: models.MessagePasswordLink,
	})
}

// UpdatePassword godoc
// @Security ApiKeyAuth
// @Summary Update Password
// @Tags auth
// @Description reset user's password account
// @Accept json
// @Produce json
// @Param data body models.UpdatePassword true "data"
// @Success 200 {object} models.ResponseOK
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /auth/update-password [patch]
func (h *handlerV1) UpdatePassword(c *gin.Context) {
	ctx, span := h.tracer.Start(c, "auth.UpdatePassword")
	defer span.End()

	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}

	userID, ok := claims["id"].(string)
	if !ok {
		h.HttpError(c, http.StatusUnauthorized, models.MessageUnauthorized, errors.New("error while getting user_id"))
		return
	}

	userEmail, ok := claims["login"].(string)
	if !ok {
		h.HttpError(c, http.StatusUnauthorized, models.MessageUnauthorized, errors.New("error while getting user email"))
		return
	}

	userInfo, err := h.strg.User().GetByID(ctx, userID)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			h.HttpError(c, http.StatusNotFound, models.MessageUserNotExists, err)
			return
		}
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	var req models.UpdatePassword
	if err := c.ShouldBindJSON(&req); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	if !isValidPassword(req.Password) {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidPassword, errors.New("weak password"))
		return
	}

	hashedPassword, err := utils.HashPassword(req.Password)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageInternalServerError, err) {
		return
	}

	if req.UserID == "" {
		req.UserID = userInfo.Id.Hex()
	}

	err = h.strg.User().UpdatePassword(ctx, userEmail, req.UserID, hashedPassword)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			h.HttpError(c, http.StatusNotFound, models.MessageUserNotExists, err)
			return
		}
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Password has been updated successfully!",
	})
}

// Register user
// @Summary	Register
// @Tags auth
// @Description this api register user
// @Accept json
// @Produce json
// @Param data body models.RegisterRequest true "data"
// @Succes 200 {object}  models.RegisterResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /auth/register  [post]
func (h *handlerV1) Register(c *gin.Context) {
	ctx, span := h.tracer.Start(c, "auth.Register")
	defer span.End()

	var body models.RegisterRequest

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	if body.UserType == models.Superadmin || (body.UserType != models.Applicant && body.UserType != models.Employer) {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserType, errors.New(models.MessageInvalidUserType))
		return
	}

	res, err := h.strg.User().GetByEmail(ctx, body.Email)
	if res != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageUserExists, errors.New(models.MessageUserExists))
		return
	}
	if err != nil && !errors.Is(err, mongo.ErrNoDocuments) {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	if !isValidPassword(body.Password) {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidPassword, errors.New(models.MessageInvalidPassword))
		return
	}

	code := utils.GenerateCode(6)
	err = email.SendEmail(h.cfg, &email.SendEmailRequest{
		To:   []string{body.Email},
		Type: email.VerificationEmail,
		Body: map[string]string{
			"code": code,
		},
		Subject: "Verification code",
	})
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageSendEmail, err)
		return
	}

	hashedPassword, err := utils.HashPassword(body.Password)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageHashedPassword, err)
		return
	}
	data := models.RequestForRedis{
		Email:     body.Email,
		Password:  hashedPassword,
		FirstName: body.FirstName,
		LastName:  body.LastName,
		UserType:  body.UserType,
		Code:      code,
	}

	dataBodyByte, err := json.Marshal(data)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageMarshalData, err)
		return
	}

	err = h.redis.SetWithTTL(body.Email, string(dataBodyByte), 600)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageSetInfoToRedis, err)
		return
	}

	c.JSON(http.StatusOK, models.RegisterResponse{
		Message: "Verification code has been sent to your email address, code is valid for 10 minutes",
	})

}

// Verfiy user
// @Summary Verify
// @Tags auth
// @Description This api verify of user
// @Accept json
// @Produce json
// @Param data body models.RequestVerify true "data"
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /auth/verify [post]
func (h *handlerV1) Verfiy(c *gin.Context) {
	ctx, span := h.tracer.Start(c, "auth.Verify")
	defer span.End()

	var user models.RequestVerify

	if err := c.ShouldBindJSON(&user); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	userBody, err := h.redis.Get(user.Email)
	if err != nil {
		h.HttpError(c, http.StatusNotFound, models.MessageGetFromRedis, err)
		return
	}
	var body models.RequestForRedis

	byteData, err := r.String(userBody, err)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageGetToString, err)
		return
	}

	err = json.Unmarshal([]byte(byteData), &body)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageErrorUnmarshal, err)
		return
	}

	if body.Email != user.Email {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidEmail, errors.New("email isn't equal to registered one"))
		return
	}

	if body.Code != user.Code {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidCode, errors.New("verfication code is wrong"))
		return
	}
	request := repo.User{
		FirstName: body.FirstName,
		LastName:  body.LastName,
		Email:     body.Email,
		Password:  body.Password,
		UserType:  body.UserType,
		CreatedAt: time.Now(),
	}

	res, err := h.strg.User().Create(ctx, &request)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageErrorCreateToUser, err)
		return
	}

	if body.UserType == models.Employer {
		_, err := h.strg.Employer().Create(ctx, &repo.Employer{
			Id:        res.(primitive.ObjectID),
			FullName:  body.FirstName + body.LastName,
			Email:     body.Email,
			Password:  body.Password,
			CreatedAt: request.CreatedAt,
		})
		if err != nil {
			h.HttpError(c, http.StatusInternalServerError, models.MessageErrorCreateToEmployer, err)
			return
		}
	} else if body.UserType == models.Applicant {
		_, err := h.strg.Applicant().Create(ctx, &repo.Applicant{
			Id: res.(primitive.ObjectID),
			Profile: repo.ApplicantProfile{
				FirstName: &body.FirstName,
				LastName:  &body.LastName,
				Email:     body.Email,
				Password:  body.Password,
			},
			CreatedAt: request.CreatedAt,
		})
		if err != nil {
			h.HttpError(c, http.StatusInternalServerError, models.MessageErrorCreateToApplicant, err)
			return
		}
	} else {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserType, errors.New("invalid user type"))
		return
	}

	userResp, err := h.strg.User().GetByEmail(ctx, body.Email)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	m := map[string]interface{}{
		"id":        userResp.Id.Hex(),
		"login":     body.Email,
		"user_type": body.UserType,
	}

	accessToken, expiresAt, err := utils.GenerateJWT(m, h.cfg.AccessTokenExpireDuration, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageGeneratingToken, err) {
		return
	}

	response := models.LoginResponse{
		UserInfo: models.User{
			ID:        userResp.Id.Hex(),
			FirstName: body.FirstName,
			LastName:  body.LastName,
			Email:     body.Email,
			UserType:  body.UserType,
			CreatedAt: request.CreatedAt,
		},
		AccessToken: accessToken,
		ExpiresAt:   expiresAt,
	}

	c.JSON(http.StatusCreated, response)
}
