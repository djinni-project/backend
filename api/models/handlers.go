package models

var (
	MessageShouldBindJSON           = "Error parsing request body. Please ensure it is a valid JSON and includes all required fields"
	MessageUserNotExists            = "User with the provided email does not exist"
	MessageInternalServerError      = "Internal server error. Please try again later"
	MessageEmailOrPasswordIsNotTrue = "Invalid email or password. Please try again"
	MessageInvalidPassword          = "Password should contain at least 6 and at most 12 characters, at least 1 number, at least 1 uppercase and 1 lowercase letter"
	MessageGeneratingToken          = "Error while generating token"
	MessagePasswordLink             = "We have sent the password reset link to your email"
	MessageUnauthorized             = "Unauthorized"
	MessageInvalidUserType          = "Invalid user type"
	MessageSendEmail                = "Code don't dispatch to email of user"
	MessageHashedPassword           = "Password isn't hashed"
	MessageMarshalData              = "Info isn't marshal"
	MessageSetInfoToRedis           = "Error set to redis data"
	MessageUserExists               = "User with the provided email exists"
	MessageGetFromRedis             = "Verification code already expired"
	MessageGetToString              = "Error get to string from redis"
	MessageErrorUnmarshal           = "Error while unmarshalling user data"
	MessageInvalidCode              = "Invalid code"
	MessageErrorCreateToEmployer    = "Error while creating employer"
	MessageErrorCreateToApplicant   = "Error while creating applicant"
	MessageInvalidEmail             = "Invalid Email"
	MessageErrorCreateToUser        = "Error while creating user"

	MessageApplicantNotExists           = "It seems like that applicant does not exist, try again later!"
	MessageApplicantSuccessFullyUpdated = "Applicant info succesfully updated!"
)

const (
	ErrInvalidEmail    = "Invalid email"
	ErrInvalidPassword = "Invalid password"
)

const (
	Employer   = "employer"
	Superadmin = "superadmin"
	Applicant  = "applicant"
)

type FailureResponse struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

type ResponseOK struct {
	Message string `json:"message"`
}
