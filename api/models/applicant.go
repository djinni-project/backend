package models

type ApplicantProfile struct {
	ID                string `json:"_id" binding:"required"`
	Email             string `json:"email" binding:"required,email"`
	Position          string `json:"position" binding:"required"`
	SalaryExpectation int64  `json:"salary_exp" binding:"required"`
	WorkExperience    struct {
		During int64  `json:"during"`
		Period string `json:"period"`
	} `json:"work_experience"`
	CountryRel        bool     `json:"country_relocation"`
	CityRel           bool     `json:"city_relocation"`
	CitiesRel         []string `json:"cities_relocation"`
	Skills            []string `json:"skills" binding:"required"`
	Category          string   `json:"category" binding:"required"`
	EnglishLevel      string   `json:"english_level"`
	EmploymentOptions []string `json:"employment_options"`
	IDontConsider     struct {
		Domains      []string `json:"domains"`
		CompanyTypes []string `json:"company_types"`
	} `json:"i_don_consider"`
	WorkExperienceDes    string   `json:"work_experience_des"`
	Expectations         string   `json:"expectations"`
	Accomplishments      string   `json:"accomplishments"`
	QuestionsForEmployer string   `json:"questions_for_employer"`
	PreferedLanguages    []string `json:"prefered_languages"`
	DesiredCommunacation string   `json:"desired_communication"`
}
