package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type LoginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=6,max=12"`
}

type LoginResponse struct {
	UserInfo    User   `json:"user_info"`
	AccessToken string `json:"access_token"`
	ExpiresAt   int64  `json:"expires_at"`
}

type ForgotPasswordRequest struct {
	Email string `json:"email" binding:"required,email"`
}

type UpdatePassword struct {
	UserID   string `json:"user_id"`
	Password string `json:"password" binding:"required,min=6,max=12"`
}

type RegisterRequest struct {
	Email     string `json:"email" binding:"required,email"`
	Password  string `json:"password" binding:"required,min=6,max=12"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UserType  string `json:"user_type"`
}

type RequestForRedis struct {
	Email     string `json:"email" binding:"required,email"`
	Password  string `json:"password" binding:"required,min=6,max=12"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UserType  string `json:"user_type"`
	Code      string `json:"code"`
}

type RegisterResponse struct {
	Message string `json:"message"`
}

type VerfiyResponse struct {
	Message string `json:"message"`
}

type VerifyUser struct {
	Id        primitive.ObjectID `json:"_id"`
	Email     string             `json:"email"`
	Password  string             `json:"password"`
	FirstName string             `json:"first_name"`
	LastName  string             `json:"last_name"`
	UserType  string             `json:"user_type"`
	CreatedAt time.Time          `json:"created_at"`
	Code      string             `json:"code"`
}

type RequestVerify struct {
	Email string `json:"email"`
	Code string `json:"code"`
}
