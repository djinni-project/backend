package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go.opentelemetry.io/otel/trace"

	"github.com/gin-contrib/cors"
	v1 "gitlab.com/djinni-project/backend/api/v1"
	"gitlab.com/djinni-project/backend/config"
	"gitlab.com/djinni-project/backend/pkg/logger"
	"gitlab.com/djinni-project/backend/storage"
	"gitlab.com/djinni-project/backend/storage/repo"
)

type RoutetOptions struct {
	Cfg     *config.Config
	Storage storage.StorageI
	Log     logger.Logger
	Tracer  trace.Tracer
	Redis   repo.RedisRepo
}

// New @title           Swagger for djinni endpoints
// @version         2.0
// @description     Djinni Backend Endpoints
// @BasePath  		/v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
// @description Type "Bearer" followed by a space and JWT token.
func New(opt *RoutetOptions) *gin.Engine {
	router := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:    opt.Cfg,
		Strg:   &opt.Storage,
		Tracer: opt.Tracer,
		Log:    opt.Log,
		Redis:  opt.Redis,
	})

	apiV1 := router.Group("/v1")

	apiV1.POST("/auth/login", handlerV1.Login)
	apiV1.POST("/auth/forgot-password", handlerV1.ForgotPassword)
	apiV1.PATCH("/auth/update-password", handlerV1.UpdatePassword)
	apiV1.POST("/auth/register", handlerV1.Register)
	apiV1.POST("/auth/verify", handlerV1.Verfiy)

	apiV1.PUT("/applicant/profile", handlerV1.UpdateApplicantProfile)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
